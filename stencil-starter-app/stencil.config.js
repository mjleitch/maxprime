const postcss = require('@stencil/postcss');
const autoprefixer = require('autoprefixer');
var atImport = require("postcss-import")
var mixins = require("postcss-mixins")
const fontMagic = require('postcss-font-magician');
const baseline = require('postcss-baseline-grid-overlay');

exports.config = {
  namespace: "maxprime",
  globalStyle: ['src/globals/main.pcss'],
  outputTargets: [
		{
			type: 'dist'
		}, {
			type: 'www'
		}
	],
  plugins: [
    postcss({
      injectGlobalPaths: [
        'src/globals/variables.pcss',
        'src/globals/mixins.pcss'
      ],
      plugins: [
        atImport(),
        baseline(),
        mixins(),
        fontMagic(),
        autoprefixer()
      ]
    })
  ]
};

exports.devServer = {
  root: 'www',
  watchGlob: '**/**'
};
