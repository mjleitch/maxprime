import { Component } from '@stencil/core';


@Component({
  tag: 'mp-heading',
  styleUrl: 'mp-heading.pcss',
  shadow: true
})
export class MpHeading {
  render() {
    return (<slot />);
  }
}
