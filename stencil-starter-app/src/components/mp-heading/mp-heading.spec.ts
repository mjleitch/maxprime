import { render } from '@stencil/core/testing';
import { MpHeading } from './mp-heading';

describe('app', () => {
  it('should build', () => {
    expect(new MpHeading()).toBeTruthy();
  });

  describe('rendering', () => {
    beforeEach(async () => {
      await render({
        components: [MpHeading],
        html: '<mp-heading></mp-heading>'
      });
    });
  });
});