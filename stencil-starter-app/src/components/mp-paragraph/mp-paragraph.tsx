import { Component, Prop } from '@stencil/core';


@Component({
  tag: 'mp-paragraph',
  styleUrl: 'mp-paragraph.pcss',
  shadow: true
})
export class MpParagraph {
  @Prop({
  reflectToAttr: true
}) lead: boolean = false;
  render() {
    return (<slot />);
  }
}
