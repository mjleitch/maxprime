import { render } from '@stencil/core/testing';
import { MpParagraph } from './mp-paragraph';

describe('app', () => {
  it('should build', () => {
    expect(new MpParagraph()).toBeTruthy();
  });

  describe('rendering', () => {
    beforeEach(async () => {
      await render({
        components: [MpParagraph],
        html: '<mp-paragraph></mp-paragraph>'
      });
    });
  });
});