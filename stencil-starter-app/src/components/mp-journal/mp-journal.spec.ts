import { render } from '@stencil/core/testing';
import { MpJournal } from './mp-journal';

describe('mp-journal', () => {
  it('should build', () => {
    expect(new MpJournal()).toBeTruthy();
  });

  describe('rendering', () => {
    beforeEach(async () => {
      await render({
        components: [MpJournal],
        html: '<mp-journal></mp-journal>'
      });
    });
  });
});