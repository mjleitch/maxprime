import { Component } from '@stencil/core';


@Component({
  tag: 'mp-journal',
  styleUrl: 'mp-journal.pcss'
})
export class MpJournal {

  render() {
    return (
      <div>
        {/* <header>
          <h1>Stencil App Starter</h1>
        </header> */}

        <main>
          <stencil-router>
            <stencil-route url='/journal' component='mp-journal-post-list' exact={true}>
            </stencil-route>

            <stencil-route url='/journal/:name' component='mp-journal-post'>
            </stencil-route>
          </stencil-router>
        </main>
      </div>
    );
  }
}
