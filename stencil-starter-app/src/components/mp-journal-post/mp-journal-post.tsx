import { Component } from '@stencil/core';


@Component({
  tag: 'mp-journal-post',
  styleUrl: 'mp-journal-post.pcss'
})
export class MpJournalPost {

  render() {
    return (
      <article>
        <header>
          <span class="mp-article-info">Jan 16, 03:41 pm</span>
        <mp-heading>
          <h1>This is a header</h1>
        </mp-heading>

      </header>
      <hr />
        <section class="mp-article-body">
          <mp-paragraph lead>When television was young, there was a hugely popular show based on the still popular fictional character of Superman.</mp-paragraph>
          <mp-paragraph>When television was young, there was a hugely popular show based on the still popular fictional character of Superman.</mp-paragraph>
        </section>
      </article>
    );
  }
}
