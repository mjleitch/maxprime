import { render } from '@stencil/core/testing';
import { MpJournalPost } from './mp-journal-post';

describe('mp-journal-post', () => {
  it('should build', () => {
    expect(new MpJournalPost()).toBeTruthy();
  });

  describe('rendering', () => {
    beforeEach(async () => {
      await render({
        components: [MpJournalPost],
        html: '<mp-journal-post></mp-journal-post>'
      });
    });
  });
});