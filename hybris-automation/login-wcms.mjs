/**
 * Login to WCMS
 *
 * This script is responsible to logging into the hybris WCMS cockpit
 * 
 * 3. goto https://admin.mauijim.prd/cmscockpit/login.zul
 */

export async function loginToWcms(page) {
  // 1. goto hybris login screen and wait for domcontentloaded
  const response = await page.goto(`https://admin.mauijim.prd/cmscockpit/login.zul`, { waitUntil: `domcontentloaded` });

  // 2.a Wait for user id input to be visible, then
  // 2.b triple click, then
  // 2.c type `mleitch`
  const userIdElHandle = await page.waitForSelector('.z-textbox[name="j_username"]', { visible: true });
  await userIdElHandle.click({ clickCount: 3 })
  await userIdElHandle.type(`mleitch`);

  // 3.a Wait for password input, then
  // 3.b triple click, then
  // 3.c type `1234`
  const passwordElHandle = await page.waitForSelector('.z-textbox[name="j_password"]', { visible: true });
  await passwordElHandle.click({ clickCount: 3 })
  await passwordElHandle.type(`1234`);

  // 4. Wait for login button, then click.
  // @TODO: this does not appear to work. It times out. What happens when I remove the visible: true?
  const loginBtnHandle = await page.waitForSelector('.z-button[title="Login"] .z-button-clk');
  await loginBtnHandle.click({ delay: 200 })

  return;
}
