/**
 * Hybris automation script to update homepage hero banner
 *
 * Below are the step for updating the homepage hero banner
 */

import puppeteer from 'puppeteer';
import dotenv from 'dotenv';

dotenv.config();

const USERNAME = process.env.HYBRIS_WCMS_USERNAME;
const PASSWORD = process.env.HYBRIS_WCMS_PASSWORD;


async function updateHomepageHeroBanner() {
    // 1. Launch a new browser
    const browser = await puppeteer.launch({ ignoreHTTPSErrors: true, headless: false, devtools: true });

    // 2. Setup new page
    const page = await browser.newPage();

    // 3. Goto login page and wait for page to finish loading
    await page.goto('https://admin.mauijim.prd/cmscockpit/login.zul', { waitUntil: `domcontentloaded` });

    // 4. Enter Username
    // 4.a Wait for user id input to be visible, then
    const userIdElHandle = await page.waitForSelector('.z-textbox[name="j_username"]');
    // 4.b Click the element
    await userIdElHandle.click({ delay: 200 });
    // 4.c wait for element to focus
    await userIdElHandle.focus()
    // 4.b triple click, then
    await userIdElHandle.click({ clickCount: 3, delay: 200 });
    // 4.c type `username`
    await userIdElHandle.type(USERNAME, { delay: 200 });

    // 4. Enter Password
    // 4.a Wait for Password input to be visible, then
    const passwordElHandle = await page.waitForSelector('.z-textbox[name="j_password"]');
    // 4.b Click the element
    await passwordElHandle.click({ delay: 200 });
    // 4.c wait for element to focus
    await passwordElHandle.focus()
    // 4.b triple click, then
    await passwordElHandle.click({ clickCount: 3, delay: 200 });
    // 4.c type `password`
    await passwordElHandle.type(PASSWORD, { delay: 200 });

    const loginBtnHandle = await page.waitForSelector('.z-button[title="Login"] .z-button');
    await loginBtnHandle.click({ delay: 200 })



    // 3. goto zeal homepage and take screenshot of hero banner
    // 4. emulate mobile and take screenshot again.
    // 5. Login to wcms
    // await loginToWcms(page);

    // 6. goto site selection screen.
    // 7. Wait for Staged Site Selector to be visible, then 
    // 8. click the `Staged` site
    // 9. Wait for the page selection screen to load
    // 10. Wait for homepage item selector then
    // 11. Double click the `homepage`
    // 12. Wait for homepage edit screen to load
    // 13. Wait for the lock icon next to Homepage Banner Slot to be visible, then double click it.
    // 14. Wait for the OK button in the "Unlock Page Template" to be visible, then click it.
    // 15. Wait for the pencil icon next to BannerComponent to be visible, then click it

    // 16.a. Wait for the text input box next to "[BannerComponent.headline]:" to be visible, then 
    // 16.b. triple click it, then
    // 16.c. type 'Meet Morrison' 

    // 17.a. Wait for the "[BannerComponent.content]:" text field to be visible, then
    // 17.b. triple click it, then
    // 17.c. type "<h2 class="secondary-hero__subtitle">Our latest Auto Sun style</h2>" 

    // 18.a. Wait for the "[BannerComponent.buttonText]:" text field to be visible, then
    // 18.b. triple click it, then
    // 18.c. type "Shop Now"

    // 19.a. Wait for the "[AbstractBannerComponent.urlLink]:" text field to be visible, then
    // 19.b. triple click it, then
    // 19.c. type "/shop/sunglasses/lifestyle/morrison?variantCode=11537"

    // 20. Update large bg image ...
    // 21. Update small bg image ...

    // 22. Synchronize homepage banner stage with online by... 
    // 23. goto zealoptics.com and take screenshot
    // 24. emulate mobile and take another screenshot
    // 25. set content of page to html img of each before and after.
    // 26. email before and after images to myself.

    // await browser.close();
    // return;
}

updateHomepageHeroBanner();


